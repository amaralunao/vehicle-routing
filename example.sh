#!/bin/bash

make virtualenv
source venv/bin/activate

docker-compose build
docker-compose up > /tmp/docker-compose.out &

python3 vehicle_routing/example_publisher.py
python3 vehicle_routing/example_consumer.py

docker-compose down
