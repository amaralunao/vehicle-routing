.PHONY: clean virtualenv requirements.txt build_requirements isort_check isort_fix flake8 test quality
DEPS:=requirements/req-base.txt
PIP=`. venv/bin/activate; which pip`

clean:
	rm -rf venv

virtualenv: clean
	virtualenv -p python3 venv
	$(PIP) install -U "pip"
	$(PIP) install -r $(DEPS)

build_requirements: virtualenv
	$(PIP) freeze -r requirements/req-base.txt > requirements.txt

requirements.txt: | build_requirements clean
	@echo "Fresh requirements"

isort_check:
	isort . --check --diff

isort_fix:
	isort .

flake8:
	flake8 --ignore D203 \
         --exclude .git,./venv/,__pycache__,docs/source/conf.py,old,build,dist \
         --max-complexity 10 \
		 --max-line-length 160

test:
	pytest

quality: flake8 isort_fix
