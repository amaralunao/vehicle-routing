# Wrapper app for optimal routes calculator (assignment)

## Local installation

Create virtualenv and install dependencies
```bash
make virtualenv
```
Activate virtualenv 
```bash
source venv/bin/activate
```

## Run the tests

In the activated virtualenv:
```bash
make test
```

## Run the example

```bash
./example.sh
```

## Run with docker (docker-compose) and test with randomly generated coordinates
```bash
docker-compose up (--build to rebuild containers)
python3 vehicle_routing/example_publisher.py
python3 vehicle_routing/example_consumer.py
```

# About the design
## Wrapper app input params example:
```
{'coordinates': [[7, 5], [5, 7]], 'depot': 0, 'num_vehicles': 1, 'event_id': 52}
```
## Wrapper app output result example:
```
b'[{"routes": [[0, 4, 1, 5, 2, 3, 8, 6, 7, 9, 0]], "event_id": 52}]'
```
* The wrapper app depends on the ```ortools``` library for calculating the routes.
* It uses ```rabbitmq message broker``` and the library ```pika``` for connections to it.
* The input values are placed on an ```input_params``` queue
* There is a consumer that listens to this queue in order to call the ortools for getting the optimal routes result
* This consumer then publishes the route results on the ```outbound``` queue from where these can be consumed by other services
* The ```event_id``` value is added to keep track of the results/match them with the coordinates.
* The testing is minimal and would need some improvement. Also dockerization is as simple as possible.
