import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="vehicle-routing-amaralunao",
    version="0.0.1",
    author="Maria Vatasoiu",
    author_email="example@example.com",
    description="Vehicle routing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/amaralunao/vehicle-routing",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
    install_requires=["ortools", "pika"],
)
