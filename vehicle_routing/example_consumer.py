#!/usr/bin/env python
import os
import sys

import pika
from retry import retry


@retry(pika.exceptions.AMQPConnectionError, delay=5, jitter=(1, 3))
def main():

    params = pika.URLParameters("amqp://admin:admin@localhost:5672/%2F")
    connection = pika.BlockingConnection(params)

    channel = connection.channel()

    channel.queue_declare(queue="outbound")

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

    channel.basic_consume(queue="outbound", on_message_callback=callback, auto_ack=True)

    print(" [*] Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
