import json
import logging

import pika
from retry import retry

from vehicle_routing.routing_wrapper import calculate_optimal_routes

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)


@retry(pika.exceptions.AMQPConnectionError, delay=5, jitter=(1, 3))
def consume():
    params = pika.URLParameters("amqp://admin:admin@localhost:5672/%2F")
    connection = pika.BlockingConnection(params)
    channel = connection.channel()

    def on_message_callback(ch, method, properties, body):
        LOGGER.info("inbound message body: %s", body)

        inbound_message = json.loads(body)
        coordinates = inbound_message["coordinates"]
        depot = inbound_message["depot"]
        num_vehicles = inbound_message["num_vehicles"]
        event_id = inbound_message["event_id"]

        routes = calculate_optimal_routes(coordinates, depot, num_vehicles)

        result = {"routes": routes, "event_id": event_id},
        outbound_message = json.dumps(result)
        LOGGER.info("outbound message body: %s", outbound_message)
        channel.basic_publish(exchange="", routing_key="outbound", body=outbound_message)

    channel.queue_declare(queue="input_param")
    channel.queue_declare(queue="outbound")
    channel.basic_consume(
        queue="input_param", on_message_callback=on_message_callback, auto_ack=True
    )

    try:
        channel.start_consuming()
    # Don't recover connections closed by server
    except pika.exceptions.ConnectionClosedByBroker:
        pass
