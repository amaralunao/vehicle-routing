#!/usr/bin/env python
import json
import random

import pika
from retry import retry

generated_coords = [[random.randint(0, 100), random.randint(0, 100)] for _ in range(10)]
input_param = {"coordinates": generated_coords, "depot": 0, "num_vehicles": 1, "event_id": random.randint(0, 100)}


@retry(pika.exceptions.AMQPConnectionError, delay=5, jitter=(1, 3))
def publish(input_param):
    params = pika.URLParameters("amqp://admin:admin@localhost:5672/%2F")
    connection = pika.BlockingConnection(params)
    channel = connection.channel()
    channel.queue_declare(queue="input_param")
    channel.basic_publish(exchange="", routing_key="input_param", body=json.dumps(input_param))
    print(" [x] Published {}".format(input_param))
    connection.close()


publish(input_param)
