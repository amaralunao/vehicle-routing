from vehicle_routing.routing_wrapper import (calculate_optimal_routes,
                                             create_distance_matrix)


class TestRoutingWrapper:

    def test_simple_calculation_success(self):
        coordinates = [[7, 6], [5, 4]]
        depot = 0
        num_vehicles = 1
        routes = calculate_optimal_routes(coordinates, depot, num_vehicles)
        assert routes == [[0, 1, 0]]

    def test_create_distance_matrix(self):
        coordinates = [[7, 9], [7, 5]]
        distance_matrix = create_distance_matrix(coordinates)
        assert distance_matrix == [[0, 4], [4, 0]]
