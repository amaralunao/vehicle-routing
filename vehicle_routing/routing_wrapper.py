import math

from ortools.constraint_solver import pywrapcp, routing_enums_pb2


def create_distance_matrix(coordinates):
    matrix = []
    for point1 in coordinates:
        row = []
        for point2 in coordinates:
            dist = int(math.dist(point1, point2))  # Euclidian distance between point1 and point2
            row.append(dist)
        matrix.append(row)
    return matrix


def calculate_optimal_routes(coord, depot, num_vehicles):
    distance_matrix = create_distance_matrix(coord)
    manager = pywrapcp.RoutingIndexManager(len(distance_matrix),
                                           num_vehicles, depot)
    routing = pywrapcp.RoutingModel(manager)

    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return distance_matrix[from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

    solution = routing.SolveWithParameters(search_parameters)
    if solution:
        return get_routes(solution, routing, manager)


def get_routes(solution, routing, manager):
    """Get vehicle routes from a solution and store them in an array."""
    # Get vehicle routes and store them in a two dimensional array whose
    # i,j entry is the jth location visited by vehicle i along its route.
    routes = []
    for route_nbr in range(routing.vehicles()):
        index = routing.Start(route_nbr)
        route = [manager.IndexToNode(index)]
        while not routing.IsEnd(index):
            index = solution.Value(routing.NextVar(index))
            route.append(manager.IndexToNode(index))
        routes.append(route)
    return routes
