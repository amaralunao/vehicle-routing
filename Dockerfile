FROM python:3.8

ADD vehicle_routing /vehicle_routing
ADD setup.py /
ADD requirements.txt /
ADD README.md /
RUN pip install -r requirements.txt
RUN python setup.py sdist bdist_wheel
RUN pip install dist/*whl

CMD [ "python", "-m", "vehicle_routing"]
